﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace B22CustomerAppPushNotification
{
    public class NewsForPushService
    {
        public async Task<List<NewsModel>> GetNewsForPush()
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync($"{Constants.NewsForPushUrl()}");

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<List<NewsModel>>(content);
                    return result;
                }

                return null;
            }
        }

        public async Task<object> PushNotify(NewsModel itemToPush)
        {

            //{
            //  "to": "/topics/my_notification_channel",
            //  "data": {
            //      "newsId": "10",
            //      "newsDateCreated": "2019-10-10 00:00:01",
            //      "newsDateToNotify": "2019-10-11 00:00:01",
            //      "newsImageUrl": "https://cdn.cfihost.com/b22/promos/B22LoyaltyAppExclusive.jpg",
            //      "newsSubTitle": "Buy cake worth Php799 and above and get another cake for free when you download our app",
            //      "newsTitle": "APP EXCLUSIVE CAKE PROMO",
            //      "newsContent": "<p>Good news for our <a href=\"https:\/\/www.facebook.com\/boulangerie22\/photos\/a.913979401984512\/2421238274591943\/?type=3&theater\">#Breadfriends!<\/a> with B22 Loyalty App!<\/p><p><a href=\"https:\/\/www.facebook.com\/hashtag\/boulangerie22?source=feed_text&amp;epa=HASHTAG&amp;__tn__=*NK-R\">#Boulangerie22<\/a> is giving you a sweet and exclusive promo that you will surely enjoy.<br \/>Buy cake worth Php799 and above and get another cake for free: Dark Chocolate Mirror Glazed Cake or Deep Dark Chocolate Cake. Make sure to present the barcode from your B22 Loyalty App to avail of the promo.<br \/>Promo runs from October 1-31, 2019.<\/p><p>Download the Boulangerie22 Loyalty App now.<\/p><p><a href=\"https:\/\/www.facebook.com\/hashtag\/bakingadifference?source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARBkfrCxip-FuQo-DCInEQY4J-N_WOeD-2H2meuF2I2O2PFEr6mLUI5GD5_o1zg52K3rylFG5YB9i6r6x_uLKl7_qFAkaLae83r3FElpMv4Dh2Y-IllOXqxDyqnR_Bk45Vs5vjiQ7u5e-UpugkLHw8GVZCwj7RMeQGGYA0oFld8Xp-IdaCwPJwmlD3bkuzqZUumTr8jQspX5WyWr2nZFa0Ityp9Xndas4uWTWEqgIbzm7sg7hPzd0oe3EpgpE_GGKNVi-1zVNvND8-Mrue7v9j8xB8L9-oxszPA7-fIYw_q_m2LbuLqyCaFJSyJA2iQP8W3j7nuEjrelJtss37UsGjfYjg&amp;__tn__=%2ANK-R\">#BakingADifference<\/a><br \/><a href=\"https:\/\/www.facebook.com\/hashtag\/cakesomeonehappy?source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARBkfrCxip-FuQo-DCInEQY4J-N_WOeD-2H2meuF2I2O2PFEr6mLUI5GD5_o1zg52K3rylFG5YB9i6r6x_uLKl7_qFAkaLae83r3FElpMv4Dh2Y-IllOXqxDyqnR_Bk45Vs5vjiQ7u5e-UpugkLHw8GVZCwj7RMeQGGYA0oFld8Xp-IdaCwPJwmlD3bkuzqZUumTr8jQspX5WyWr2nZFa0Ityp9Xndas4uWTWEqgIbzm7sg7hPzd0oe3EpgpE_GGKNVi-1zVNvND8-Mrue7v9j8xB8L9-oxszPA7-fIYw_q_m2LbuLqyCaFJSyJA2iQP8W3j7nuEjrelJtss37UsGjfYjg&amp;__tn__=%2ANK-R\">#CakeSomeoneHappy<\/a><\/p>"
            //  },
            //  "notification": {
            //      "title": "APP EXCLUSIVE CAKE PROMO",
            //      "body": "Buy cake worth Php799 and above and get another cake for free when you download our app"
            //  },
            //  "priority": 10
            //};

            var dataToPush = new
            {
                to = Constants.TopicPath,
                data = new
                {
                    newsId = itemToPush.Id,
                    newsDateCreated = itemToPush.DateCreated,
                    newsDateToNotify = itemToPush.DateToNotify,
                    newsImageUrl = itemToPush.ImageUrl,
                    newsSubTitle = itemToPush.SubTitle,
                    newsTitle = itemToPush.Title,
                    newsContent = itemToPush.Content
                },
                notification = new
                {
                    title = itemToPush.Title,
                    body = itemToPush.SubTitle
                },
                priority = 10
            };

            var jsonObject = JsonConvert.SerializeObject(dataToPush);
            var payload = new StringContent(jsonObject, Encoding.UTF8, "application/json");
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", $"key={Constants.GetFcmKey()}");

                var response = await client.PostAsync(Constants.FcmUrl(), payload);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject(content);
                    return result;
                }

                return null;
            }

        }

    }
}
