﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B22CustomerAppPushNotification
{
    public static class Constants
    {
        public static string NewsForPushUrl() { return GetBaseUrl() + "NewsForPush"; }
        public static string FcmUrl() { return "https://fcm.googleapis.com/fcm/send"; }
        public const string FirebaseUrl = "https://boulangerie22-1489574063607.firebaseio.com/";
        public const int TransactionPageSize = 10;
        public static string GetFcmKey() 
        {            
            return ConfigurationManager.AppSettings["FirebaseServerSecretKey"].ToString();
        }

#if DEBUG
        public const string TopicPath = "/topics/not_a_real_channel";
#else
        public const string TopicPath = "/topics/my_notification_channel";
#endif

        public static string GetBaseUrl()
        {
            return "https://b22api.cfihost.com/api/";
        }
    }

}
