﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B22CustomerAppPushNotification
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            try
            {

                log4net.GlobalContext.Properties["LogName"] = "B22CustomerAppPushNotification";
                log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo("log4net.config"));

                var newsForPushService = new NewsForPushService();
                log.Info($"Retrieving the news for push notification today ({DateTime.Now.ToShortDateString()})");
                var result = newsForPushService.GetNewsForPush().Result;
                var resultForPrinting = JsonConvert.SerializeObject(result, Formatting.Indented);
                log.Info($"There are {result.Count} news postings for push notification today");
                log.Info(resultForPrinting);

                foreach (var item in result)
                {
                    log.Info($"Trying to push notify \"{item.Title}\"");
                    var pushResult = newsForPushService.PushNotify(item).Result;

                    if (pushResult == null)
                    {
                        log.Error("There is a problem executing Push Notification, please check the " +
                            "B22CustomerAppPushNotification.config file if contains the correct key");                        
                    } 
                    else
                    {
                        log.Info(pushResult.ToString());
                    }
                    
                }

                log.Info("Finished push notification");

            }
            catch (Exception ex)
            {
                log.Error(ex);
            } 
            finally
            {
#if DEBUG
                Console.ReadKey();
#endif
            }

        }


    }
}
